package org.komar.bus.service;

import org.junit.Before;
import org.junit.Test;
import org.komar.bus.service.impl.RouteServiceReverse;

import java.io.InputStreamReader;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RouteServiceTest {

    private RouteService routeService;

    @Before
    public void init() {
        routeService = new RouteServiceReverse();
        InputStreamReader inputStreamReader = new InputStreamReader(this.getClass().getResourceAsStream("/testdata.rou"));
        routeService.init(inputStreamReader);
    }

    @Test
    public void test() {
        assertTrue(routeService.checkDirect(3, 6));
        assertTrue(routeService.checkDirect(4, 0));
        assertTrue(routeService.checkDirect(1, 5));
        assertFalse(routeService.checkDirect(2, 6));
        assertFalse(routeService.checkDirect(222, 6));
        assertFalse(routeService.checkDirect(3, 222));
        assertFalse(routeService.checkDirect(444, 222));
    }
}