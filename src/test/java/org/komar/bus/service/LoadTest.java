package org.komar.bus.service;

import org.junit.Before;
import org.junit.Test;
import org.komar.bus.service.impl.RouteServiceReverse;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;

import static org.junit.Assume.assumeNotNull;

public class LoadTest {

    private RouteService routeService;

    @Before
    public void init() {
        Instant start = Instant.now();
        routeService = new RouteServiceReverse();
        InputStream resource = this.getClass().getResourceAsStream("/big10k.rou");
        if (resource == null) {
            System.out.println("Big test file not found. Maybe you need to use Generator to create it");
        }
        assumeNotNull(resource);
        InputStreamReader inputStreamReader = new InputStreamReader(resource);
        routeService.init(inputStreamReader);
        Instant end = Instant.now();
        System.out.println("Load time: " + Duration.between(start, end).toString());
    }

    @Test
    public void test() {
        check(3, 6);
        check(34242, 32226);
        check(10, 32226);
        check(105, 4000);
        check(163343, 23);
        check(888999, 999888);
        check(22123, 4426);
        check(5453422, 23232);
        check(5453422, 23232);
        check(5453422, 23232);
        check(5453422, 23232);
        check(673964, 328747);
        check(-1, 213);
        check(212321, -1);
    }

    private void check(int stationId1, int stationId2) {
        Instant start = Instant.now();
        boolean res = routeService.checkDirect(stationId1, stationId2);
        Instant end = Instant.now();
        System.out.println("Check time: " + Duration.between(start, end).toString() + " result: " + res);
    }
}