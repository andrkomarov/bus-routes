package org.komar.bus.vert;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import org.komar.bus.service.RouteService;
import org.komar.bus.service.impl.RouteServiceReverse;

import java.io.FileReader;

public class Server extends AbstractVerticle {

    private final FileReader reader;

    public Server(FileReader reader) {
        this.reader = reader;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        RouteService service = new RouteServiceReverse();
        // To switch straightforward implementation uncomment next line and comment previous
        // RouteService service = new RouteServiceArray();
        try {
            service.init(reader);
        } catch (NumberFormatException e) {
            System.out.println("There was a problem parsing file. Check if it contains digits only");
            startFuture.fail("");
        } catch (Exception e) {
            System.out.println("Something went wrong");
            e.printStackTrace();
            startFuture.fail("");
        }

        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);
        Route route = router.route().path("/api/direct");

        route.handler(routingContext -> {
            HttpServerRequest request = routingContext.request();
            HttpServerResponse response = request.response();
            String depSid = request.getParam("dep_sid");
            String arrSid = request.getParam("arr_sid");

            if (depSid == null || arrSid == null) {
                response.end("Error reading parameters arr_sid, dep_sid");
            } else {
                try {
                    int sid1 = Integer.valueOf(depSid);
                    int sid2 = Integer.valueOf(arrSid);
                    boolean direct = service.checkDirect(sid1, sid2);
                    System.out.println(String.format("Requested stations: %d, %d. Result: %s", sid1, sid2, direct));
                    response.putHeader("content-type", "application/json");
                    response.end(
                            "{\n" +
                                    "    \"dep_sid\": " + depSid + ",\n" +
                                    "    \"arr_sid\": " + arrSid + ",\n" +
                                    "    \"direct_bus_route\": " + direct + "\n" +
                                    "}"
                    );
                } catch (NumberFormatException e) {
                    response.end("Error parsing parameters: " + e.getMessage());
                }
            }
        });

        server.requestHandler(router::accept).listen(8088);
        startFuture.complete();
    }
}
