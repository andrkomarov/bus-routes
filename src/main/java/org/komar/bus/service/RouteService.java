package org.komar.bus.service;

import java.io.Reader;

public interface RouteService {
    void init(Reader reader) throws NumberFormatException;

    boolean checkDirect(int stationId1, int stationId2);
}
