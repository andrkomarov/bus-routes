package org.komar.bus.service.impl;

import org.komar.bus.service.RouteService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

public class RouteServiceArray implements RouteService {

    private int[][] routes;

    public void init(Reader reader) throws NumberFormatException {
        try (BufferedReader inputStream = new BufferedReader(reader)) {
            String l;
            int loadedRouteCount = 0;
            int plannedRouteCount = Integer.valueOf(inputStream.readLine());
            routes = new int[plannedRouteCount][];
            while ((l = inputStream.readLine()) != null) {
                String[] routeStr = l.split(" ");
                if (routeStr.length >= 3) {
                    int[] route = new int[routeStr.length - 1];
                    for (int i = 1; i < routeStr.length; i++) {
                        route[i - 1] = Integer.valueOf(routeStr[i]);
                    }
                    Arrays.sort(route);
                    routes[loadedRouteCount] = route;
                    loadedRouteCount++;
                } else {
                    System.out.println("Skipping following line as it contains less than 3 ids: " + l);
                }
            }

            System.out.println(String.format("Loaded %d routes vs %d planned", loadedRouteCount, plannedRouteCount));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean checkDirect(int stationId1, int stationId2) {
        return Arrays.stream(routes)
                .parallel()
                .filter(r -> Arrays.binarySearch(r, stationId1) >= 0 && Arrays.binarySearch(r, stationId2) >= 0)
                .findAny().isPresent();
    }
}
