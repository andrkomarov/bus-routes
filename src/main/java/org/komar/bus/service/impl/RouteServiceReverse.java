package org.komar.bus.service.impl;

import org.komar.bus.service.RouteService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RouteServiceReverse implements RouteService {

    Map<Integer, StationData> stationIndex = new HashMap<>();
    private int[][] routes;
    private final int STATION_ARRAY_BATCH = 100000;
    private final int ROUTE_ARRAY_BATCH = 10;

    public void init(Reader reader) throws NumberFormatException {
        try (BufferedReader inputStream = new BufferedReader(reader)) {
            String l;
            int loadedRouteCount = 0;
            int stationNr = 0;
            int plannedRouteCount = Integer.valueOf(inputStream.readLine());
            routes = new int[STATION_ARRAY_BATCH][];
            while ((l = inputStream.readLine()) != null) {
                String[] routeStr = l.split(" ");
                if (routeStr.length >= 3) {
                    int routeId = Integer.valueOf(routeStr[0]);
                    for (int i = 1; i < routeStr.length; i++) {
                        Integer stationId = Integer.valueOf(routeStr[i]);
                        StationData data = stationIndex.get(stationId);
                        if (data == null) {
                            if (stationNr != 0 && stationNr % STATION_ARRAY_BATCH == 0) {
                                routes = Arrays.copyOf(routes, routes.length + STATION_ARRAY_BATCH);
                            }
                            data = new StationData(stationNr++, 1);
                            stationIndex.put(stationId, data);
                        } else {
                            stationIndex.put(stationId, data.increment());
                        }
                        int[] stationRoutes = routes[data.index];
                        if (stationRoutes == null) {
                            stationRoutes = new int[ROUTE_ARRAY_BATCH];
                            stationRoutes[0] = routeId;
                        } else {
                            if (data.size % ROUTE_ARRAY_BATCH == 0) {
                                stationRoutes = Arrays.copyOf(stationRoutes, stationRoutes.length + ROUTE_ARRAY_BATCH);
                            }
                            stationRoutes[data.size - 1] = routeId;
                        }
                        routes[data.index] = stationRoutes;
                    }

                    loadedRouteCount++;
                } else {
                    System.out.println("Skipping following line as it contains less than 3 ids: " + l);
                }
            }
            System.out.println(String.format("Loaded %d routes vs %d planned", loadedRouteCount, plannedRouteCount));
            System.out.println(String.format("Loaded %d stations. Sorting...", stationNr));
            routes = Arrays.copyOf(routes, routes.length - (STATION_ARRAY_BATCH - stationNr % STATION_ARRAY_BATCH));
            stationIndex.values().parallelStream().forEach(sd -> {
                int index = sd.index;
                int size = sd.size;
                routes[index] = Arrays.copyOf(routes[index], routes[index].length - (ROUTE_ARRAY_BATCH - size % ROUTE_ARRAY_BATCH));
                Arrays.sort(routes[index]);
            });
            System.out.println("Let's rock!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean checkDirect(int stationId1, int stationId2) {
        StationData sd1 = stationIndex.get(stationId1);
        StationData sd2 = stationIndex.get(stationId2);
        return !(sd1 == null || sd2 == null) &&
                Arrays.stream(routes[sd1.index])
                        .parallel()
                        .filter(rId -> Arrays.binarySearch(routes[sd2.index], rId) >= 0)
                        .findAny().isPresent();
    }

    private class StationData {
        private int index;
        private int size;

        public StationData(int index, int size) {
            this.index = index;
            this.size = size;
        }

        public StationData increment() {
            this.size++;
            return this;
        }
    }
}
