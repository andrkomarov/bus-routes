package org.komar.bus;

import io.vertx.core.Vertx;
import org.komar.bus.vert.Server;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Starter {

    public static void main(String... args) {
        if (args.length == 0) {
            System.out.println("You should specify routes file");
            return;
        }

        String filepath = args[0];
        FileReader reader;
        try {
            reader = new FileReader(filepath);
        } catch (FileNotFoundException e) {
            System.out.println("File '" + filepath + "' was not found");
            return;
        }
        Vertx.vertx().deployVerticle(new Server(reader), r -> {
            if (r.succeeded()) {
                System.out.println("Server deployed successfully");
            } else {
                System.out.println("Server start failed");
                System.exit(1);
            }
        });
    }
}