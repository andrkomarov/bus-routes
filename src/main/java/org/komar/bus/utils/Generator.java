package org.komar.bus.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Generator {

    public static void main(String... args) {
        PrintWriter outputStream = null;

        int routes = 100000;
        int maxStationPerRoute = 1000;
        int maxStationId = 100000;
        try {
            outputStream = new PrintWriter(new FileWriter("big1m.rou"));
            outputStream.println(routes);

            for (int i = 0; i < routes; i++) {
                List<String> list = new ArrayList<>();
                int stations = ThreadLocalRandom.current().nextInt(2 * maxStationPerRoute / 3, maxStationPerRoute);
                for (int j = 0; j < stations; j++) {
                    list.add(String.valueOf(ThreadLocalRandom.current().nextInt(0, maxStationId + 1)));
                }
                outputStream.println(i + " " + list.stream().collect(Collectors.joining(" ")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
}
