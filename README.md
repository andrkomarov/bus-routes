# README #

Default implementation is a little bit complex but it shows ~10 times faster response time comparing with straightforward RouteServiceArray.
Though we are paying for that with longer file load time. It takes about a minute on my old laptop for 100000x1000 route file.
Memory consumption can also be optimized a bit changing Map data structure.
If you really want to check straightforward RouteServiceArray implementation (5 times faster load time, but 10 times slower response) please change line 25 of Server class.
